import os
import re
import sys

use_file = "example.txt"
use_file = "input.txt"

total = 0

with open(use_file, "r") as fp:
  lines = list(map(lambda x: x.strip(), fp.readlines()))

def find_x(origin_line_id, origin_char_id):
  left_slash = (
    (origin_line_id - 1, origin_char_id - 1),
    (origin_line_id + 1, origin_char_id + 1)
  )
  right_slash = (
    (origin_line_id - 1, origin_char_id + 1),
    (origin_line_id + 1, origin_char_id - 1)
  )
  all_neighbors = [
    left_slash[0][0],
    left_slash[0][1],
    left_slash[1][0],
    left_slash[1][1],
    right_slash[0][0],
    right_slash[0][1],
    right_slash[1][0],
    right_slash[1][1],
  ]
  # No wraparound
  if not all(map(lambda x: x >= 0, all_neighbors)):
    return False
  try:
    left_slash = [lines[left_slash[0][0]][left_slash[0][1]], lines[left_slash[1][0]][left_slash[1][1]]]
    right_slash = [lines[right_slash[0][0]][right_slash[0][1]], lines[right_slash[1][0]][right_slash[1][1]]]
  except IndexError:
    return False
  if not("M" in left_slash and "S" in left_slash):
    return False
  if "M" in right_slash and "S" in right_slash:
    return True
  return False

for line_id in range(len(lines)):
  for char_id in range(len(lines[line_id])):
    this_char = lines[line_id][char_id]
    if this_char == "A" and find_x(line_id, char_id):
      total += 1

print(total)
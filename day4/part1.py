import os
import re
import sys

use_file = "example.txt"
use_file = "input.txt"

total = 0

# Expressed as (line_offset, char_offset)
DIRECTIONS = [
  (-1, 0),
  (-1, 1),
  (0, 1),
  (1, 1),
  (1, 0),
  (1, -1),
  (0, -1),
  (-1, -1),
]

with open(use_file, "r") as fp:
  lines = list(map(lambda x: x.strip(), fp.readlines()))

def find_words(origin_line_id, origin_char_id):
  # import pdb;pdb.set_trace()
  for direction in DIRECTIONS:
    m_line_id = origin_line_id + direction[0]
    m_char_id = origin_char_id + direction[1]
    a_line_id = origin_line_id + direction[0]*2
    a_char_id = origin_char_id + direction[1]*2
    s_line_id = origin_line_id + direction[0]*3
    s_char_id = origin_char_id + direction[1]*3
    if not all(map(lambda x: x >= 0, [m_line_id, m_char_id, a_line_id, a_char_id, s_line_id, s_char_id])):
      continue
    try:
      m_char = lines[m_line_id][m_char_id]
      a_char = lines[a_line_id][a_char_id]
      s_char = lines[s_line_id][s_char_id]
    except IndexError:
      continue
    if m_char != "M":
      continue
    if a_char != "A":
      continue
    if s_char != "S":
      continue
    yield (s_line_id, s_char_id)

for line_id in range(len(lines)):
  for char_id in range(len(lines[line_id])):
    this_char = lines[line_id][char_id]
    if this_char == "X":
      total += len(list(find_words(line_id, char_id)))

print(total)
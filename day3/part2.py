import os
import re
import sys

use_file = "example2.txt"
use_file = "input.txt"

total = 0
do_mul = True

lineno = 0

#114961848 too high
#
#82232708 too low
#74963672 too low

with open(use_file, "r") as fp:
    mem_line = 'init'
    while mem_line:
        lineno += 1
        mem_line = fp.readline()
        print(lineno)
        if not mem_line:
            break

        for op_match in re.finditer("(do)\\(\\)|(don)'t|mul\\((\\d{1,3}),(\\d{1,3})\\)", mem_line):
            if op_match.group(1):
                do_mul = True
            elif op_match.group(2):
                do_mul = False
            elif do_mul:
                total += int(op_match.group(3)) * int(op_match.group(4))

print(total)
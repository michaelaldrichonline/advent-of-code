import os
import re
import sys

use_file = "example.txt"
use_file = "input.txt"

total = 0

with open(use_file, "r") as fp:
    mem_line = 'init'
    while mem_line:
        mem_line = fp.readline()
        if not mem_line:
            break
        for match in re.finditer("mul\\((\\d{1,3}),(\\d{1,3})\\)", mem_line):
            total += int(match.group(1)) * int(match.group(2))
            

print(total)
import os
import re
import sys
import math

use_file = "example.txt"
use_file = "input.txt"

total = 0

rules = {}
updates = []
# This says 75 must be after 45
# rules[75]["after"].append(45)

parsing_rules = True

with open(use_file, "r") as fp:
    mem_line = fp.readline()
    while mem_line:
        if mem_line.strip() == "":
          parsing_rules = False
        elif parsing_rules:
          left, right = mem_line.strip().split("|")
          rules.setdefault(left, {"before":set(), "after":set()})
          rules.setdefault(right, {"before":set(), "after":set()})
          rules[left]["before"].add(right)
          rules[right]["after"].add(left)
        else:
          updates.append(mem_line.strip().split(","))
        mem_line = fp.readline()

def sort_update(unsorted):
  # print(unsorted)
  unsorted = unsorted[:]
  done_sorting = []
  while unsorted:
    potential_next = unsorted.pop(0)
    if rules[potential_next]["after"].isdisjoint(unsorted):
      done_sorting.append(potential_next)
    else:
      unsorted.append(potential_next)
  # print(done_sorting)
  return done_sorting

for update in updates:
  # print(update)
  for i, page in enumerate(update):
    # print(page)
    if not rules.get(page):
      continue
    # print(rules[page])
    # input()
    if rules[page]["after"].intersection(update[i:]):
      sorted_update = sort_update(update)
      total += int(sorted_update[(len(sorted_update) - 1)//2])
      break
    if rules[page]["before"].intersection(update[:i]):
      sorted_update = sort_update(update)
      total += int(sorted_update[(len(sorted_update) - 1)//2])
      break

print(total)
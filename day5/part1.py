import os
import re
import sys
import math

use_file = "example.txt"
use_file = "input.txt"

total = 0

rules = {}
updates = []
# This says 75 must be after 45
# rules[75]["after"].append(45)

parsing_rules = True

with open(use_file, "r") as fp:
    mem_line = fp.readline()
    while mem_line:
        if mem_line.strip() == "":
          parsing_rules = False
        elif parsing_rules:
          left, right = mem_line.strip().split("|")
          rules.setdefault(left, {"before":set(), "after":set()})
          rules.setdefault(right, {"before":set(), "after":set()})
          rules[left]["before"].add(right)
          rules[right]["after"].add(left)
        else:
          updates.append(mem_line.strip().split(","))
        mem_line = fp.readline()

for update in updates:
  # print(update)
  for i, page in enumerate(update):
    # print(page)
    if not rules.get(page):
      continue
    # print(rules[page])
    # input()
    # If any pages required to be after are before, bad news.
    if rules[page]["after"].intersection(update[i:]):
      # print('before contains after')
      break
    if rules[page]["before"].intersection(update[:i]):
      # print('after contains before')
      break
  else:
    # print('adding ' + str(update[(len(update) - 1)//2]))
    total += int(update[(len(update) - 1)//2])
    # input()

print(total)
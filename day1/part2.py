import os
import re
import sys

left_column = []
right_column = []

use_file = "example.txt"
use_file = "input.txt"

with open(use_file, "r") as fp:
    line = 'init'
    while line:
        line = fp.readline()
        match = re.match("(\\d+) +(\\d+)", line)
        if not match:
            continue
        left_column.append(int(match.group(1)))
        right_column.append(int(match.group(2)))

left_column.sort()
right_column.sort()

similarity = 0

for left_num, right_num in zip(left_column, right_column):
    similarity += left_num * right_column.count(left_num)

print(similarity)
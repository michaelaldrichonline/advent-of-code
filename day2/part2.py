import os
import re
import sys

use_file = "example.txt"
use_file = "input.txt"

safe_report_count = 0

def check_levels(levels):
    if not ((levels == sorted(levels)) or (levels == list(reversed(sorted(levels))))):
        return False
    for i in range(len(levels)-1):
        delta = abs(levels[i + 1] - levels[i])
        if delta > 3:
            return False
        if delta < 1:
            return False
    return True

with open(use_file, "r") as fp:
    report = 'init'
    while report:
        report = fp.readline()
        if not report:
            break
        levels = list(map(int, report.split(" ")))
        if check_levels(levels):
            safe_report_count +=1
            continue

        more_levels = []
        for i in range(len(levels)):
            if check_levels(levels[:i]+levels[i+1:]):
                safe_report_count += 1
                break
        # print(levels)
        # input()

print(safe_report_count)
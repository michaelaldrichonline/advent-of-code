import os
import re
import sys

use_file = "example.txt"
use_file = "input.txt"

safe_report_count = 0

with open(use_file, "r") as fp:
    report = 'init'
    while report:
        report = fp.readline()
        if not report:
            break
        levels = list(map(int, report.split(" ")))
        # print(levels)
        # input()
        if not ((levels == sorted(levels)) or (levels == list(reversed(sorted(levels))))):
            # print('not sorted')
            continue
        for i in range(len(levels)-1):
            delta = abs(levels[i + 1] - levels[i])
            if delta > 3:
                # print('too much delta')
                break
            if delta < 1:
                # print('not enough delta')
                break
        else:
            # print("good")
            safe_report_count += 1
print(safe_report_count)